// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FLWGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FLW_API AFLWGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
